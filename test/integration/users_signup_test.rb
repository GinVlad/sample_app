require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest
  test "invalid signup invalid" do
    get signup_path #Lấy thông tin từ trang singup và sử dụng phương thức post để thực hiện kiểm tra
    #Xác nhận giá trị "User.count" không thay đổi kể cả trước và sau khi thực hiện lệnh
    #Như before_save = User.count == after_save = User.count
    assert_no_difference 'User.count' do
      post users_path, user: { name: "",
                               email: "email@invalid",
                               password: "123",
                               password_confirmation: "456" }
    end
    assert_template 'users/new' #Kiểm tra hiện diện của trang /user/new
    #Kiểm tra sự hiện diện của các thông báo lỗi
    assert_select 'div#<CSS id for error explanation>'
    assert_select 'div.<CSS class for field with error>'
  end
  
  #Test nếu dữ liệu đúng có save không
  test "valid signup information" do
    get signup_path
    #Kiểm tra khác nhau trước và sau, ở đây là trước thì User.count = 0 và sau là 1
    assert_difference 'User.count', 1 do
      #Phương thức post về users_path nếu save thành công
      post_via_redirect users_path, 
      user: { name: "Example User",
              email: "user@example.com",
              password: "password",
              password_confirmation: "password" }
    end
    assert_template 'users/show'
    #Kiểm tra flash phải được thêm vào không được rỗng
    assert_not flash.nil?
    #Cách khác assert_not_nil flash
  end
end
