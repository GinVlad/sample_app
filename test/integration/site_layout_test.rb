require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest
  test 'layout links' do
    #Phương thức assert_select dùng để kiểm tra sự hiện diện trong view
    get root_path #Dẫn tới đường link root
    assert_template 'static_pages/home' #Kiểm tra layout của /home đã được render chưa
    assert_select "a[href=?]", root_path, count:2 #Kiểm tra số lần xuất hiện của <a href='home'>, ở đây là 2 lần
    assert_select "a[href=?]", help_path #Kiểm tra số lần xuất hiện của <a href='help'>
    assert_select "a[href=?]", about_path #Kiểm tra số lần xuất hiện của <a href='about'>
    assert_select "a[href=?]", contact_path #Kiểm tra số lần xuất hiện của <a href='contact'>
    
    #Để test sử dụng lệnh: bundle exec rake test:integration
  end
end
