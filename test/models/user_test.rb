require 'test_helper'

class UserTest < ActiveSupport::TestCase
  
  def setup
    @user = User.new(name: "Example User", email: "user@example.com", 
                        password: "foobar", password_confirmation: "foobar")
  end
  
  test "should do something" do
    assert @user.valid? #Method assert(xác nhận), để xác nhận biến object @user có tồn tại hay không
  end
  
  #test nến biến @name là rỗng thì xác_nhận_không tồn tại
  #Sau đó sử dụng hàm validates(attributes, options hash) để xác nhận kiểm tra trong models/user.rb
  test "name should be present" do
    @user.name = "   " #test biến @user là rỗng
    assert_not @user.valid? #xác nhận không có @user tồn tại
  end
  
  #test nến biến @email là rỗng thì xác_nhận_không tồn tại
  #Sau đó sử dụng hàm validates(attributes, options hash) để xác nhận kiểm tra trong models/user.rb
  test "email should be present" do
    @user.email = "     "
    assert_not @user.valid?
  end
  
  #test xem thử biến @name có dài quá 50 ký tự hay không
  test "name should not be to long" do
    @user.name = "a" * 51
    assert_not @user.valid?
  end
  
  #test xem thử biến @email có dài quá 255 ký tự hay không
  test "email should not be to long" do
    @user.email = "a" * 256
    assert_not @user.valid?
  end
  
  #test xem thử biến @email có thuộc các dạng chuẩn không
  test "email validation should accept valid address" do 
    valid_addresses = %w[user@example.com USER@foo.COM A_USER@foo.bar.org first.last@foo.jp alice+bob@gmail.com]
    valid_addresses.each do |valid_address|
      @user.email = valid_address
      assert @user.valid? , "#{valid_address} should be valid"
    end
  end
  
  #test xem thử biến @email có thuộc các dạng không chuẩn
  test "email validation should reject invalid addresses" do
    invalid_addresses = %w[user@example,com user_at_foo.org user.name@example. foo@bar_baz.com foo@bar+baz.com foo@bar..com]
    invalid_addresses.each do |invalid_address|
      @user.email = invalid_address
      assert_not @user.valid?, "#{invalid_address.inspect} should be invalid"
    end
  end
  
  #test xem thử biến @email có bị t rùng hay không
  test "email addresses should me unique" do
    #Tạo một biến @duplicate_user để trung với @user
    duplicate_user = @user.dup
    #Chuyển email của @duplicate_user về dạng in hoa, để tránh các trường hợp foo@bar.com và FOO@BAR.COM
    duplicate_user.email = @user.email.upcase
    @user.save
    assert_not duplicate_user.valid?
  end
  
  #test @email có bằng @email đã save có lower-case không
  test "email addresses should be saved as lower-case" do
    mixed_case_email = "Foo@ExAMPle.CoM"
    @user.email = mixed_case_email
    @user.save
    assert_equal mixed_case_email.downcase, @user.reload.email
    #Phương thức reload để reload lại giá trị trong database trước khi thay đổi
  end
  
  #Test độ dài tối thiểu của mật khẩu
  test "password should have a minimum length" do
    @user.password = @user.password_confirmation = "a" * 5
    assert_not @user.valid?
  end
  
end
