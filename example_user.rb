class User
    attr_accessor :name, :email #1 biến tương ứng cho đối tượng name và email, để khởi tạo getter và setter từ instance variables
    
    #Biến đối tượng được khai báo luôn luôn có @. Ex: @variable
    #Tạo hàm khởi tạo trong Class User nhận vào 1 hashes rỗng, sau đó gán giá trị cho các đối tương tương ứng
    def initialize(attributes = {})
        @name = attributes[:name]
        @email = attributes[:email]
    end
    
    def formatted_email
        "#{@name} <#{email}>"
    end
end

#Run in console:
#>> require './example_user'
#=> true
#>> example = User.new
#=> #<User:0x007fb5eded61b0 @name=nil, @email=nil>
#>> example.name
#=> nil
#>> example.name = "GinVlad"
#=> "GinVlad"
#>> example.email = "ginvlad@gmail.com-"
#=> "ginvlad@gmail.com-"
#>> example.formatted_email
#=> "GinVlad <ginvlad@gmail.com->"