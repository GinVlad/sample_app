class UsersController < ApplicationController
  def show
    #Tại biến instance @use nhận giá trị từ model User.find(id). params[:id] nhận id từ url vào, vì hàm sử dụng phương thức GET
    #params[:id] nhận vào string. nhưng find() nhận ra được string hoặc lại integer
    @user = User.find(params[:id])
    #debugger
    #có thể sử dụng từ khoá debugger. để bật chế độ debug trên rails with rails log.
  end
  
  def new
    #Khởi tạo biến @user từ đổi tượng User
    @user = User.new
  end
  
  def create
    #@user = User.new(params[:user])  #Tạo mới user bằng cách lấy dữ liệu với phương thức params[:user]
    #Cách trên không được vì người dùng có thể insert dữ liệu vào tất các cột kể cả các cột không hiển thị ra ngoài
    #Nếu để trống và nhập sẽ báo lỗi toàn bộ cột của table đó, không handle được.
    #Cách này sử dụng hàm user_params, để giới hạn lại các cột cần nhập, các cột khác nhập vào sẽ báo lỗi
    @user = User.new(user_params)
    if @user.save
      #Sử dụng phương thức tạo thông báo là flash{hash}, ở đây tạo thông báo flash {success: "Welcome to the Sample APP!"}
      flash[:success] = "Welcome to the Sample App!"
      #Phương thức redirect_to @user, trả về link /user/:id (tương ứng với control #show)
      redirect_to @user   #Same as: redirect_to users_url(@user)
    else
      render 'new' #Nếu save không thành công chuyển lại về controller new, bằng phương thức render
    end
  end
  
  #Khởi tạo block private chỉ để sử dụng trong User_controller
  private
    #Hàm user_params để trả về các giá trị cho phép nhập vào bới phương thức params.
    def user_params
      # .require(@hash) yêu cầu với đối tượng nào đó
      # .permit(attributes) cho phép các thuộc tính của đối tượng trên được khai báo trong này hoặc động
      params.require(:user).permit(:name, :email, :password, :password_confirmation)
      #hàm trả về các attributes được khai báo
    end
  
end