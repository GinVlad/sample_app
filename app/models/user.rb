class User < ActiveRecord::Base
    #Sử dụng phương thức before_save để chuyển các email nhập vào xuống dạng chữ thường bằng .downcase
    before_save { email.downcase! }
    #phương thức validates, sử dụng options hash, validates biết @name, đưa vào argument: presence: true
    validates :name, presence: true, length: { maximum: 50 }
    #validates(:name, presence: true) #Cách viết khác
    
    #constant trong ruby bắt đầu bằng ký tự in hoa
    #Tạo regexp trên http://rubular.com. Sử dụng để valid email. Sử dụng hash format trong validates() method
    VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
    validates(:email, presence: true, length: { maximum: 255 }, 
                            format: { with: VALID_EMAIL_REGEX },
                            uniqueness: { case_sensitive: false })
                            #case_sensitive: false để kiểm tra các mục trung lặp có nằm trong các dạng đặc biệt hay không
    
    #Sử dụng phương thức has_secure_password. Để tạo ra password hash, 1 phương thức của rails
    #Users: column_name: password_digest. create 2 password and password_confirmation
    #Nó sử dụng gem bcrypt để tạo các password_hashed
    #http://api.rubyonrails.org/classes/ActiveModel/SecurePassword/ClassMethods.html#method-i-has_secure_password
    has_secure_password
    validates :password, length: { minimum: 6 }
end
