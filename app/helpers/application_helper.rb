module ApplicationHelper
    #Return a full title in on a per-page basics.
    def full_title(page_title = '')
        base_title = "Ruby on Rails Tutorial Sample App" 
        if page_title.empty?
           base_title
        else
            "#{page_title} | #{base_title}" #lấy biến page_title, base_title điền vào 
        end
    end
end
