class AddPasswordDigestToUsers < ActiveRecord::Migration
  def change
    #phương thức add_column được dùng để tạo column password_digest:string vào table users
    add_column :users, :password_digest, :string
  end
end
