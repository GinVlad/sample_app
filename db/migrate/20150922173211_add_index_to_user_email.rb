#rails generate migration add_index_to_user_email
#Để add index vào csdl, làm cho phần tìm kiếm email theo index dễ dàng
#sử dụng option unique: true để thực hiện index được uniqueness
class AddIndexToUserEmail < ActiveRecord::Migration
  def change
    add_index :users, :email, unique: true 
  end
end

#Chạy migration giữa database và migration: bundle rake exec db:migrate
#If fail, need turn of all rails sandbox still running