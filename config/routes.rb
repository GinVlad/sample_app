Rails.application.routes.draw do
  
  # You can have the root of your site routed with "root"
  # Gọi tới trang statis_page/home làm trang chủ
  # Vài đã tạo biết home_path, và home_urh
  root 'static_pages#home'
  

  #Sử dụng cách đặt tên theo dạng *_path, *_url, lấy theo hàm get để việc gọi tới các đường dẫn bằng hàng link_to ở các file html dễ hơn
  #mapping tới static_pages_controller.rb và tại hàm help 
  #Ta có help_path và help_url
  get 'help' => 'static_pages#help' 
  
  #mapping tới static_pages_controller.rb và tại hàm home 
  #Ta có about_path và about_url
  get 'about' => 'static_pages#about' 
  
  #Ta có contact_path và contact_url
  get 'contact' => 'static_pages#contact'
  
  #get 'users/new' Chuyển sang kiểu khai báo để có dạng users_path, users_url
  #Tạo đường link /signup, không cần phải là /users/new
  get 'signup' => 'users#new' #Route tự động tạo ra signup_path và signup_url

  #Phương thức resources :model. Dùng để mapping các đường dẫn với data của model.
  #Sử dụng command: rake routes để kiểm tra
  resources :users
end
